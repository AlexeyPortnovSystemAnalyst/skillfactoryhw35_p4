package ru.sf;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ChannelYoutubePage {

    public final WebDriver webDriver;
    private final static String FIRST_CHANNEL = "ytd-item-section-renderer";//style-scope ytd-item-section-renderer
//yt-simple-endpoint style-scope yt-formatted-string

    public ChannelYoutubePage (WebDriver webDriver) {
        this.webDriver = webDriver;
    }


    public String getFirstChannel() {
       webDriver.findElement(By.xpath("/html/body/ytd-app/div/ytd-page-manager/ytd-search/div[1]/ytd-two-column-search-results-renderer/div/ytd-section-list-renderer/div[2]/ytd-item-section-renderer[4]/div[3]/ytd-video-renderer[1]/div[1]/ytd-thumbnail/a/yt-img-shadow/img")).click();////*[@id="contents"]/ytd-item-section-renderer[4]
//yt-simple-endpoint style-scope yt-formatted-string
      //webDriver.findElement(By.className("yt-simple-endpoint")).click();
        //Artsiom Rusau QA Life
       return webDriver.findElement(By.xpath("/html/body/ytd-app/div/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[7]/div[2]/ytd-video-secondary-info-renderer/div/div/ytd-video-owner-renderer/div[1]/ytd-channel-name/div/div/yt-formatted-string/a")).getText();

    }



}
