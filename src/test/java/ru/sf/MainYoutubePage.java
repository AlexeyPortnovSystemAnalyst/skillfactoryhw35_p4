package ru.sf;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class MainYoutubePage {

    public final WebDriver webDriver;
    private static final String SEARCH_FIELD_CLASS = "gsfi";
    private static final String ERROR_MESSAGE_SPAN_CLASS = "locality-selector-popup__table-empty-text";
    private static final String URL = "https://youtube.ru/";


    public MainYoutubePage (WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void go() {
        webDriver.get(URL);
    }

    public void searchLesson(String lesson) {
        webDriver.findElement(By.className("ytd-searchbox-spt")).click();
        webDriver.findElement(By.className(SEARCH_FIELD_CLASS)).sendKeys(lesson, Keys.ENTER);
    }

//    public String getCityNotFoundMessage() {
//        return webDriver.findElement(By.className(ERROR_MESSAGE_SPAN_CLASS)).getText();
//    }

}
