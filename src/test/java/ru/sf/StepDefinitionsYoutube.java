package ru.sf;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.Assert.assertEquals;

public class StepDefinitionsYoutube {

    public static final WebDriver webDriver;
    public static final MainYoutubePage MAIN_YOUTUBE_PAGE;
    public static final ChannelYoutubePage CHANNEL_YOUTUBE_PAGE;

    //Процесс инициализации необходимых ресурсов
    static {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\89672\\OneDrive\\Документы\\projects\\QAJA_m35_cc-scenario\\src\\test\\resources\\chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        webDriver.manage().window().maximize();
        MAIN_YOUTUBE_PAGE = new MainYoutubePage(webDriver);
        CHANNEL_YOUTUBE_PAGE = new ChannelYoutubePage(webDriver);
    }

    @Given("me main youtube page")
    public void me_main_youtube_page() {
        MAIN_YOUTUBE_PAGE.go();
    }

    @Then("find QA lessons {string}")
    public void find_qa_lessons(String lesson) {
        MAIN_YOUTUBE_PAGE.searchLesson(lesson);
    }

    @Then("assert that chosen lessons is {string}")
    public void assert_that_chosen_lessons_is(String string) {
        assertEquals("Artsiom Rusau QA Life",CHANNEL_YOUTUBE_PAGE.getFirstChannel());
    }

    @Then("assert that user got message {string}")
    public void assert_that_user_got_message(String errorMessage) {
        assertEquals("Artsiom Rusau QA Life",CHANNEL_YOUTUBE_PAGE.getFirstChannel());
    }
}
