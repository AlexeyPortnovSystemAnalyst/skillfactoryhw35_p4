#Gherkin
Feature: Find QA lessons
  #Позитивный сценарий: выбираем канал с курсом "Тестировщик с нуля" с канала Артем Русай
  Scenario: finding QA lessons on Youtube
    Given me main youtube page
    Then  find QA lessons 'Тестировщик с нуля'
    And assert that chosen lessons is 'Artsiom Rusau QA Life'

      #Негативный сценарий: выбираем урок "Тестировщик PRO" и это не канал Артем Русай
  Scenario: finding QA lessons on Youtube
    Given me main youtube page
    Then  find QA lessons 'Тестировщик PRO'
    And assert that user got message 'Урок не найден'